import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import Home from './home';
import List from './list';
import Char from './char';
import Item from './item';

const Stack = createStackNavigator()

function MainStackNavigator() {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName='Home'
        screenOptions={{
          gestureEnabled: true,
          headerStyle: {
            backgroundColor: '#101010'
          },
          headerTitleStyle: {
            fontWeight: 'bold'
          },
          headerTintColor: '#ffffff',
          headerBackTitleVisible: false
        }}
        headerMode='float'>
        <Stack.Screen
          name='Home'
          component={Home}
          options={{ title: 'Home Screen' }}
        />
        <Stack.Screen
          name='List'
          component={List}
          options={{ title: 'Lista de personajes' }}
        />
        <Stack.Screen
          name='Item'
          component={Item}
          options={{ title: 'Lista de items' }}
        />
        <Stack.Screen
          name='Char'
          component={Char}
          options={({ route }) => ({
            title: route.params.actor.name
          })}
        />
      </Stack.Navigator>
    </NavigationContainer>
  )
}

export default MainStackNavigator