import React from 'react'
import { StyleSheet, View, Text, TouchableOpacity, Image, SafeAreaView, FlatList } from 'react-native'
import Items from '../data/Items.json'
import Icons from '../img/export';

function Item(props) {
        
    const Data = Items;
    
    return (
            <SafeAreaView  style={{margin:12, backgroundColor: '#f8f8ff'}}>
                <Text style={{ fontSize: 32, textAlign: 'center', fontWeight: 'bold',}}>LISTA DE iTEMS DE PROJECT TWILIGHT</Text>
                <FlatList  
                    data={Data}
                    numColumns={2}
                    renderItem={({ item }) => (
                        <View key={item.key} style={styles.box}>
                                <Image
                                    style={styles.tinyLogo}
                                    source={Icons[item.url]}
                                />
                                <View style={{ paddingLeft: 10}}>
                                    <Text style={styles.titulo}>{item.name}</Text>
                                    <Text >Tipo de equipamiento:</Text>
                                    <Text style={styles.desc}>{item.equip}</Text>
                                    <View style={{flexDirection: 'row'}}>
                                        <Image style={styles.icon}
                                            source={require('../img/attack.png')}
                                        />
                                        <Text style={styles.stat}>+ {item.stats[0]}</Text>
                                    </View>
                                    <View style={{flexDirection: 'row'}}>
                                        <Image style={styles.icon}
                                            source={require('../img/defense.png')}
                                        />
                                         <Text style={styles.stat}>+ {item.stats[1]}</Text>
                                    </View>
                                    <View style={{flexDirection: 'row'}}>
                                        <Image style={styles.icon}
                                            source={require('../img/magicattack.png')}
                                        />
                                       <Text style={styles.stat}>+ {item.stats[2]}</Text>
                                    </View>
                                    <View style={{flexDirection: 'row'}}>
                                        <Image style={styles.icon}
                                            source={require('../img/magicdeffense.png')}
                                        />
                                        <Text style={styles.stat}>+ {item.stats[3]}</Text>
                                    </View>
                                    <View style={{flexDirection: 'row'}}>
                                        <Image style={styles.icon}
                                            source={require('../img/heart.png')}
                                        />
                                        <Text style={styles.stat}>+ {item.stats[4]}</Text>
                                    </View>
                                    <View style={{flexDirection: 'row'}}>
                                        <Image style={styles.icon}
                                            source={require('../img/mp.png')}
                                        />
                                        <Text style={styles.stat}>+ {item.stats[5]}</Text>
                                    </View>                                  
                                </View>
                            </View>
                    )}
                    keyExtractor={item => item.id}
                />  
            </SafeAreaView >
    )
}

const styles = StyleSheet.create({
   
box: {  
    flex: 1,
    justifyContent: 'space-between',
    padding: 8,
    margin: 5,
    alignContent: 'stretch',
    backgroundColor: '#ffffe0',
    borderColor: 'gray', borderWidth: 5,
},
titulo: {  
    fontWeight: 'bold',
    fontSize: 30,
    marginBottom: 10
},
desc: {
    fontSize: 20,
    marginBottom: 5
},
stat: {
    fontSize: 16,
    fontWeight: 'bold',
    alignItems: 'flex-end'
},
tinyLogo: {
    borderWidth: 3,
    borderRadius: 10,
    borderColor: '#ffd700',
    height: 100,
    width: 100,
  },
  text: {
    color: '#ffffff',
    fontSize: 24,
    fontWeight: 'bold'
  }, icon: {height: 25, width: 25}
})

export default Item